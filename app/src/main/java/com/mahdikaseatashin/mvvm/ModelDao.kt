package com.mahdikaseatashin.mvvm

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ModelDao {

    @Insert
    fun insert(model: Model)

    @Update
    fun update(model: Model)

    @Delete
    fun delete(model: Model)

    @Query("SELECT * FROM tb_models ORDER BY id DESC")
    fun getAllModels() : LiveData<List<Model>>
}
