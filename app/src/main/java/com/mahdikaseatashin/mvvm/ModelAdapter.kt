package com.mahdikaseatashin.mvvm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION


class ModelAdapter(private val onItemClickListener: (Model) -> Unit)
    : ListAdapter<Model, ModelAdapter.ModelHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.model_item, parent,
            false)
        return ModelHolder(itemView)
    }

    override fun onBindViewHolder(holder: ModelHolder, position: Int) {
        with(getItem(position)) {
            holder.tvTitle.text = title
            holder.tvDescription.text = description
        }
    }

    fun getNoteAt(position: Int): Model = getItem(position)


    inner class ModelHolder(iv: View) : RecyclerView.ViewHolder(iv) {

        val tvTitle: TextView = itemView.findViewById(R.id.text_view_title)
        val tvDescription: TextView = itemView.findViewById(R.id.text_view_description)

        init {
            itemView.setOnClickListener {
                if(adapterPosition != NO_POSITION)
                    onItemClickListener(getItem(adapterPosition))
            }
        }

    }
}

private val diffCallback = object : DiffUtil.ItemCallback<Model>() {
    override fun areItemsTheSame(oldItem: Model, newItem: Model) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Model, newItem: Model) =
        oldItem.title == newItem.title
                && oldItem.description == newItem.description
}
